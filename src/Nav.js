import React, { useState, useEffect } from 'react'
import "./Nav.css";
import logo from "./Netflix_2014_logo.svg";
import profile from "./profile-pic.png";

function Nav() {
	const [show, handleShow] = useState(false);
	useEffect(() => {
		window.addEventListener("scroll", () => {
			handleShow(window.scrollY > 100);
		});
		return  () => {
			window.removeEventListener("scroll");
		};
	}, []);

	return (
		<div className={`nav ${show && "nav__black"}`}>
			<img className="nav__logo"
				src={logo}
				alt="Netflix Logo"
			/>

			<img className="nav__avatar"
				src={profile}
				alt="Profile"
			/>
		</div>
	)
}

export default Nav
